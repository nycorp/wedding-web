@if ($paginator->hasPages())
    <div class="section-pagination">
        <div class="row">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())



                <div class="col-xs-4">
                    <a href="#">&lt;&lt; Previous</a>
                </div>
            @else
                <div class="col-xs-4">
                    <a href="{{ $paginator->previousPageUrl() }}">&lt;&lt; Previous</a>
                </div>
            @endif

            {{-- Pagination Elements --}}
            <div class="col-xs-4 text-center">
														<span class="pagination-numbers">
        @foreach ($elements as $element)
                                                                {{-- "Three Dots" Separator --}}
                                                                @if (is_string($element))

                                                                @endif

                                                                {{-- Array Of Links --}}
                                                                @if (is_array($element))
                                                                    @foreach ($element as $page => $url)
                                                                        @if ($page == $paginator->currentPage())
                                                                            <a class="active"><span>{{ $page }}</span></a>
                                                                        @else
                                                                            <a href="{{ $url }}">{{ $page }}</a>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                            									</span>
            </div>

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <div class="col-xs-4 text-right">
                    <a href="{{ $paginator->nextPageUrl() }}">Next &#62;&#62;</a>
                </div>
            @else
                <div class="col-xs-4 text-right">
                    <a href="#">Next &#62;&#62;</a>
                </div>
            @endif
        </div>
    </div>
@endif
