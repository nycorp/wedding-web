@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
        <div class="registration_3">
            <div class="contact-form">
                <!-- Form -->
                <form action="{{url('admin/timeline/store')}}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="">
                    <div class="content_box">
                        <img id="profile_" class="img-responsive"
                             alt="" height="100" width="100">
                        <input type="file" name="picture"
                               id="picture_"
                               onchange="readURL(this,'profile_')"
                               accept="image/*" style="display:none"/>

                        <div class="grid_1 simpleCart_shelfItem">
                            <div class="item_add">
                                        <span class="item_price">
                                            <label for="picture_">Choose picture</label>
                                        </span>
                            </div>
                        </div>
                    </div>

                    <div>
                        <span><label for="title">Title</label></span>
                        <span> <input type="text" id="title" tabindex="1" name="title"
                                      required="required"
                                      placeholder="Title"/></span>
                        @if ($errors->has('title'))
                            <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div>
                        <span> <label for="sub_title">Sub-Title</label></span>
                        <span>  <input type="text" id="sub_title" name="sub_title" tabindex="2"
                                       placeholder="Sub-Title"/></span>
                        @if ($errors->has('sub_title'))
                            <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('sub_title') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="sky-form">
                        <div class="sky_form1">
                            <ul>
                                @if(!empty($couple) or $couple !== null)
                                    @foreach($couple as $person)
                                        <li><label class="radio left"><input type="radio" name="couple_id" value="{{$person->id}}"
                                                                             required=""><i></i>{{$person->lastName}}
                                            </label></li>
                                    @endforeach
                                @endif
                                <div class="clearfix"></div>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <span> <label for="comment">Comment</label></span>
                        <span><textarea id="comment" name="comment" tabindex="3"
                                        placeholder="Comment"></textarea></span>
                        @if ($errors->has('comment'))
                            <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('comment') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div>
                        <input type="submit" value="Save">
                    </div>
                </form>
                <!-- /Form -->
            </div>
        </div>
        @if(!empty($timelines) or $timelines !== null)
            @foreach($timelines as $timeline)
                <div class="registration_3">
                    <div class="contact-form">
                        <!-- Form -->
                        <form action="{{url('admin/timeline/update')}}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$timeline->id}}">
                            <div class="content_box">
                                <img src="{{asset($timeline->photo)}}" id="profile_{{$timeline->id}}"
                                     class="img-responsive"
                                     alt="" height="100" width="100">
                                <input type="file" name="picture"
                                       id="picture_{{$timeline->id}}"
                                       onchange="readURL(this,'profile_{{$timeline->id}}')"
                                       accept="image/*" style="display:none"/>

                                <div class="grid_1 simpleCart_shelfItem">
                                    <div class="item_add">
                                        <span class="item_price">
                                            <label for="picture_{{$timeline->id}}">Choose picture</label>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <span><label for="title">Title</label></span>
                                <span> <input type="text" id="title" tabindex="1" name="title"
                                              required="required"
                                              placeholder="Title" value="{{$timeline->title}}"/></span>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <span> <label for="sub_title">Sub-Title</label></span>
                                <span>  <input type="text" id="sub_title" name="sub_title" tabindex="2"
                                               placeholder="Sub-Title" value="{{$timeline->sous_title}}"/></span>
                                @if ($errors->has('sub_title'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="sky-form">
                                <div class="sky_form1">
                                    <ul>
                                        @if(!empty($couple) or $couple !== null)
                                            @foreach($couple as $person)
                                                <li><label class="radio left"><input type="radio" name="couple_id" value="{{$person->id}}"
                                                                                     required=""
                                                                                     @if($person->id==$timeline->couples_id) checked="" @endif><i></i>{{$person->lastName}}
                                                    </label></li>
                                            @endforeach
                                        @endif
                                        <div class="clearfix"></div>
                                        @if ($errors->has('couples_id'))
                                            <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('couples_id') }}</strong>
                                    </span>
                                    </ul>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <span> <label for="comment">Comment</label></span>
                                <span><textarea id="comment" name="comment" tabindex="3"
                                                placeholder="Comment">{{$timeline->comment}}</textarea></span>
                                @if ($errors->has('comment'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('comment') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <input type="submit" value="Update">
                            </div>
                        </form>
                        <!-- /Form -->
                    </div>
                </div>
            @endforeach
        @endif
        <div class="clearfix"></div>
    </div>

@endsection

@section('javascript')
    {{-- to display image on select --}}
    <script type="text/javascript">
        function readURL(input, image) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById(image).src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
