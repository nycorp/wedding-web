@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
        <div class="registration_left">
            <div class="widget dashboard-container my-adslist">
                <h3 class="widget-header">Nos Tables</h3>
                <table class="table table-responsive product-dashboard-table">
                    <thead>
                    <tr>
                        <th>Nom des tables</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($table as $tab)
                        <tr>
                            <td class="product-details">
                                <ul>
                                        <li><strong>{{$tab->nom}}</strong>
                                        </li>

                                </ul>
                            </td>
                            <td class="action" data-title="Action">
                                <div class="">
                                    <ul class="list-inline justify-content-center">
                                        <li class="list-inline-item">
                                            <a class="edit" href="{{url('admin/timeline/store')}}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table></div>
        </div>
        <div class="clearfix"></div>
        <div>

        </div>
    </div>
    <a class="pull-right" href="{{ url('/rsvp/') }}"><strong>Ajouter Guest</strong></a></br></br>
@endsection