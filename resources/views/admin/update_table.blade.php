@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
        <div class="registration_left">
            <div class="contact-form">
                <!-- Form -->
                <form action="{{url('admin/table/edit',$table->id)}}" method="POST"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}


                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input type="text" name="nom" class="form-control" id="nom"  required
                               value="{{$table->nom}}">
                        <div>
                            @if ($errors->has('nom'))
                                <span class="data-error red-text">
                                        <strong>{{ $errors->first('nom') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div>
                        <input type="submit" value="Update">
                    </div>
                </form>
                <!-- /Form -->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection