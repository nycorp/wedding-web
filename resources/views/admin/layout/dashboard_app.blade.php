<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>{{ config('app.name', 'Duplexe & Wendy') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Gretong Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css'/>
    <!-- Custom CSS -->
    <link href="{{asset('dashboard/css/style.css')}}" rel='stylesheet' type='text/css'/>
    <!-- Graph CSS -->
    <link href="{{asset('dashboard/css/font-awesome.css')}}" rel="stylesheet">
    <!-- jQuery -->
    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <!-- /google web fonts --><!-- lined-icons -->
    <link rel="stylesheet" href="{{asset('dashboard/css/icon-font.min.css')}}" type='text/css'/>
    @yield('stylesheet')
    <script src="{{asset('dashboard/js/simpleCart.min.js')}}"></script>
    <script src="{{asset('dashboard/js/amcharts.js')}}"></script>
    <script src="{{asset('dashboard/js/serial.js')}}"></script>
    <script src="{{asset('dashboard/js/light.js')}}"></script>
    <!-- //lined-icons -->
    <script src="{{asset('dashboard/js/jquery-1.10.2.min.js')}}"></script>
    <!--pie-chart--->
    <script src="{{asset('dashboard/js/pie-chart.js')}}" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });


        });

    </script>
</head>
<body>
<div class="page-container">
    <!--/content-inner-->
    <div class="left-content">
        <div class="inner-content">
            <!-- header-starts -->
            <div class="header-section">
                <!-- top_bg -->
                <div class="top_bg">

                    <div class="header_top">
                        <div class="top_right">
                            <ul>
                                <li><a href="{{route('homes')}}">Back to {{ config('app.name', 'Duplexe & Wendy') }}
                                        website</a></li>
                            </ul>
                        </div>
                        <div class="top_left">
                            <ul>
                                @if (Auth::guard('admin')->check())
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ url('/admin/logout') }}"
                                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <!-- /top_bg -->
            </div>

            <!--content-->
            <div class="content">
                <div class="women_main">
                    <!-- start content -->
                @yield('content')
                <!-- end content -->
                </div>
            </div>
            <!--content-->
        </div>
    </div>
    <!--//content-inner-->
    <!--/sidebar-menu-->
    <div class="sidebar-menu">
        <header class="logo1">
            <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
        </header>
        <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
        <div class="menu">
            <ul id="menu">
                <li><a href="{{ url('admin/home') }}"><i class="fa fa-home"></i> <span>Home</span></a></li>
                <li id="menu-academico"><a href="#"><i class="fa fa-info-circle"></i> <span> About us</span> <span
                                class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub">
                        <li id="menu-academico-avaliacoes"><a href="{{ url('admin/couple') }}">Couple</a></li>
                        <li id="menu-academico-avaliacoes"><a href="{{ url('admin/timeline') }}">Timeline</a></li>
                    </ul>
                </li>
                <li id="menu-academico"><a href="#"><i class="fa fa-info-circle"></i> <span> Tables</span> <span
                                class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub">
                        <li id="menu-academico-avaliacoes"><a href="{{ url('admin/table') }}">Create table</a></li>
                        <li id="menu-academico-avaliacoes"><a href="{{ url('admin/liste') }}">Update table</a></li>
                        <li id="menu-academico-avaliacoes"><a href="{{ url('admin/assigner') }}">Assigned table</a></li>
                    </ul>
                </li>
                <li><a href="sunglasses.html"><i class="fa fa-file-text-o"></i>
                        <span>Sunglasses</span></a></li>
                <li><a href="{{ url('admin/guest') }}"><i class="lnr lnr-pencil"></i> <span>Invités Enrégistrés</span></a></li>
                <li id="menu-academico"><a href="catalog.html"><i class="fa fa-file-text-o"></i>
                        <span>Catalog</span></a></li>
                <li id="menu-academico"><a href="shoes.html"><i class="lnr lnr-book"></i> <span>Shoes</span></a></li>
                <li><a href="bags.html"><i class="lnr lnr-envelope"></i> <span>Bags</span></a></li>
                <li><a href="products.html"><i class="lnr lnr-chart-bars"></i> <span>Watches</span></a></li>
                <li id="menu-academico"><a href="#"><i class="lnr lnr-layers"></i> <span>Tabs & Calender</span> <span
                                class="fa fa-angle-right" style="float: right"></span></a>
                    <ul id="menu-academico-sub">
                        <li id="menu-academico-avaliacoes"><a href="tabs.html">Tabs</a></li>
                        <li id="menu-academico-boletim"><a href="calender.html">Calender</a></li>

                    </ul>
                </li>
                <li><a href="#"><i class="lnr lnr-chart-bars"></i> <span>Forms</span> <span class="fa fa-angle-right"
                                                                                            style="float: right"></span></a>
                    <ul>
                        <li><a href="input.html"> Input</a></li>
                        <li><a href="validation.html">Validation</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    var toggle = false;

    $(".sidebar-icon").click(function () {
        if (toggle) {
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position": "absolute"});
        }
        else {
            $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
            setTimeout(function () {
                $("#menu span").css({"position": "relative"});
            }, 400);
        }

        toggle = !toggle;
    });
</script>
<!--js -->
<script src="{{asset('dashboard/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('dashboard/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('dashboard/js/bootstrap.min.js')}}"></script>
<!-- /Bootstrap Core JavaScript -->
<!-- real-time -->
<script language="javascript" type="text/javascript" src="{{asset('dashboard/js/jquery.flot.js')}}"></script>
<script src="{{asset('dashboard/js/menu_jquery.js')}}"></script>
@yield('javascript')
</body>
</html>