@extends('admin.layout.dashboard_app')
@section('content')
    <div class="page-content">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- page title -->
                <h2 class="title1 couple-title1 title1-border">Admin login</h2>
                <!-- /page title -->
                <div class="row">
                    <!-- RSVP -->
                    <div class="col-sm-4 rsvp-text">
                        <h3 class="title4">Please login and give the modification if you want</h3>
                        <p>Kindly respond by December 29, 2018. We look forward to celebrating with you!</p>
                    </div>
                    <div class="col-sm-8">
                        <form id="contactForm" method="POST" action="{{ url('/admin/login') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email">Your Email</label>
                                <input type="email" id="email" name="email" class="form-control required"
                                       placeholder="Email" value="{{ old('email') }}"/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Your password</label>
                                <input type="password" id="password" name="password" class="form-control required"
                                       placeholder="Password"/>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group form-send registration_form">
                                <div>
                                    <input type="submit" value="sign in">
                                </div>
                                <div class="forget">
                                    <a href="{{ url('/admin/password/reset') }}">forgot your password</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
