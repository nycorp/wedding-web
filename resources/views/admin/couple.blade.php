@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
        @if(!empty($couple) or $couple !== null)
            @foreach($couple as $person)
                <div class="registration_left">
                    <h2>{{$person->title}} <span> information </span></h2>
                    <div class="contact-form">
                        <!-- Form -->
                        <form action="{{url('admin/couple')}}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$person->id}}">
                            <div class="content_box">
                                <img src="{{asset($person->photo)}}" id="profile_{{$person->id}}" class="img-responsive"
                                     alt="">
                                <input type="file" name="picture"
                                       id="picture_{{$person->id}}"
                                       onchange="readURL(this,'profile_{{$person->id}}')"
                                       accept="image/*" style="display:none"/>

                                <div class="grid_1 simpleCart_shelfItem">
                                    <div class="item_add">
                                        <span class="item_price">
                                            <label for="picture_{{$person->id}}">Choose picture</label>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <span><label for="first_name">First name</label></span>
                                <span> <input type="text" id="first_name" tabindex="1" name="first_name"
                                              required="required"
                                              placeholder="First name" value="{{$person->firstName}}"/></span>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <span> <label for="last_name">Last name</label></span>
                                <span>  <input type="text" id="last_name" name="last_name" tabindex="2"
                                               required="required"
                                               placeholder="Last name" value="{{$person->lastName}}"/></span>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <span> <label for="comment">Comment</label></span>
                                <span><textarea id="comment" name="comment" tabindex="3" required="required"
                                                placeholder="Comment">{{$person->comment}}</textarea></span>
                                @if ($errors->has('comment'))
                                    <span class="help-block">
                                        <strong class="alert-danger">{{ $errors->first('comment') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div>
                                <input type="submit" value="Update">
                            </div>
                        </form>
                        <!-- /Form -->
                    </div>
                </div>
            @endforeach
        @endif
        <div class="clearfix"></div>
    </div>

@endsection

@section('javascript')
    {{-- to display image on select --}}
    <script type="text/javascript">
        function readURL(input, image) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById(image).src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
