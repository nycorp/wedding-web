@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
                <div class="registration_left">
                    <div class="contact-form">
                        <!-- Form -->
                        <form action="{{url('admin/table')}}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}


                            <div class="control-group">
                                <label class="control-label" for="input11">Nom de la table</label>
                                <div class="controls">
                                    <input type="text" class="span4" id="input11" name="nom"
                                           value="{{$nom or old('nom')}}"
                                           required>
                                    <div>
                                        @if ($errors->has('nom'))
                                            <span class="data-error">
                                        <strong>{{ $errors->first('nom') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div>
                                <input type="submit" value="Create">
                            </div>
                        </form>
                        <!-- /Form -->
                    </div>
                </div>
        <div class="clearfix"></div>
    </div>

@endsection