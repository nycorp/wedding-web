﻿@extends('admin.layout.dashboard_app')

@section('content')
    <div class="registration">
        <div class="registration_3">
            <div class="contact-form">
                <!-- Form -->
                <form class="contactForm" method="POST" role="form" action="{{ url('/guest/register') }}">
                {{ csrf_field() }}
                <!-- Form Field -->
                    <!-- /Form Field -->
                    <div class="row">


                        <!-- item -->
                        <div class="form-group">
                            <label for="guest">Name of all guest:</label>
                            <select id="guest" name="guest" class="form-control">

                                <option value="0">All Guest</option>
                                @foreach($guest as $gues)
                                    <option value="{{$gues->id}}">{{$gues->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            @if ($errors->has('guest'))
                                <span class="data-error">
                                        <strong>{{ $errors->first('guest') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <!-- Form Field -->
                        <!-- item -->
                        <div class="form-group">
                            <label for="table">Name of table</label>
                            <select id="table" name="table" class="form-control">
                                <option value="">All Tables</option>
                                @foreach($table as $tab)
                                    <option value="{{$tab->id}}">{{$tab->nom}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            @if ($errors->has('table'))
                                <span class="data-error">
                <strong>{{ $errors->first('table') }}</strong>
            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-send">
                        <button type="submit" value="" class="btn btn-1c"><span class="fa fa-paper-plane"> &nbsp;</span>Assigner
                        </button>
                        <!--<input type="submit" value="Send" class="btn btn-1c"/>-->
                    </div>
                    <!-- /Form Field -->
                </form>
                <!-- /Form -->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

@endsection
