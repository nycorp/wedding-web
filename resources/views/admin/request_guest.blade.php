@extends('admin.layout.dashboard_app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <div class="panel panel-default">
                    <div class="panel-heading">INVITES ENREGISTRES</div>

                        <table class="table table-responsive ">
                            <thead>
                            <tr>
                                <th>Numero</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Message</th>
                                <th>Number of Guest</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div hidden>
                                {{$i=1}}
                            </div>
                            @foreach($guest as $gues )

                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$gues->name or ''}}</td>
                                    <td>{{$gues->email or ''}}</td>
                                    <td>{{$gues->message or ''}}</td>
                                    <td style="text-align: center">{{$gues->number_guests or ''}}</td>

                                </tr><br>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>

@endsection

