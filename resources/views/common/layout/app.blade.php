<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }}</title>

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{asset("assets/img/logo1.png")}}" type="image/x-icon">
    <link rel="icon" href="{{asset("assets/img/logo1.png")}}" type="image/x-icon">
    <!-- / Favicons
    ================================================== -->

    <!-- >> CSS
    ============================================================================== -->
    <!-- google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <!-- /google web fonts -->
    <!-- bootstrap -->
    <link href="{{asset("assets/vendor/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- nivo lightbox -->
    <link href="{{asset("assets/vendor/nivo-lightbox/nivo-lightbox.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("assets/vendor/nivo-lightbox/themes/default/default.css")}}" type="text/css"/>
    <!-- animate -->
    <link href="{{asset("assets/vendor/animate.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("assets/vendor/font-awesome/css/font-awesome.min.css")}}">
    <!-- main styles -->
    <link href="{{asset("assets/css/styles.css")}}" rel="stylesheet">
@yield('stylesheet')
<!-- >> /CSS

============================================================================== -->
</head>
<body>

<!-- Page Loader
========================================================= -->
<div class="loader" id="page-loader">
    <div class="loading-wrapper">
        <div class="loader-heart loader-heart1"><img src="{{asset("assets/img/intro-heart.png")}}" alt=""></div>
        <div class="loader-heart loader-heart2"><img src="{{asset("assets/img/intro-heart.png")}}" alt=""></div>
        <div class="loader-heart loader-heart3"><img src="{{asset("assets/img/intro-heart.png")}}" alt=""></div>
    </div>
</div>
<!-- /End of Page loader
========================================================= -->

<!-- === PAGE BACKGROUND === -->
<!-- Background container -->
<div class="page-background-container viewport">
    <!-- background-mask -->
    <div class="bg-mask viewport"></div>
    <!-- /background-mask -->
    <!-- background-slideshow -->
    <div class="bg-slideshow">
        <!-- slideshow-item -->
        <div class="page-background viewport" style="background-image: url({{asset('assets/img/couples-picturess1.png')}})"></div>

    </div>

    <!--/Background-slideshow -->
</div>
<!-- Background-container -->
<!-- === /PAGE BACKGROUND === -->

<!-- Main Content

================================================== -->
<div id="body" class="viewport animated">
    <div class="container">
        <div class="boxed-content">
            <!-- Header -->
            <header id="header" class="navbar navbar-default navbar-static-top">
                <div class="clearfix">
                    <div>
                        <ul>
                            @if (Auth::guard('guest')->check())
                                <li style="float: right; top: 100px; margin: 10px">
                                    <a href="{{ url('/guest/logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Guest Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/guest/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @elseif(Auth::guard('admin')->check())
                                <li style="float: right; top: 100px; margin: 10px">
                                    <a href="{{ url('/admin/logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Admin Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <!-- logo -->
                    <div class="hd-logo">
                        <a href="{{route('homes')}}"><img src="{{asset("assets/img/logo1.png")}}" title="" alt=""></a>
                    </div>
                    <!-- /logo -->
                    <!-- responsive menu -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Navigation Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive menu -->
                    <!-- menu -->
                    <nav class="hd-nav nav navbar-nav navbar-right navbar-collapse collapse">
                        <ul class="nav navbar-nav hd-list-menu clearfix">
                            <li>
                                <a href="{{route('about')}}">
                                    <span class="link-title">About</span>
                                    <span class="link-subtitle">our history</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('gallery')}}">
                                    <span class="link-title">Gallery</span>
                                    <span class="link-subtitle">Our pictures</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('ceremony')}}">
                                    <span class="link-title">Ceremony</span>
                                    <span class="link-subtitle">All information</span>
                                </a>
                            </li>
                            <li>
                                @if(!Auth::guard('guest')->check())
                                <a href="{{route('code')}}">
                                    <span class="link-title">RSVP</span>
                                    <span class="link-subtitle">Events Attending</span>
                                </a>
                                @endif
                            </li>
                            <li>
                                <a href="{{route('blog')}}">
                                    <span class="link-title">Blog</span>
                                    <span class="link-subtitle">All the news!</span>
                                </a>

                            </li>
                            <li>
                                <a href="{{route('registry')}}">
                                    <span class="link-title">Registry</span>
                                    <span class="link-subtitle">Our Gift List</span>
                                </a>
                            </li>


                        </ul>
                    </nav>
                    <!-- /menu -->
                </div>
            </header>
            <!-- /header -->
            <!-- content -->
        @yield('content')
        <!-- /content -->
            <!-- footer -->
            <footer id="footer">
                <p class="footer-icon"><img src="{{asset("assets/img/ft-icon.png")}}" alt=""></p>
                <p class="quote-content">"When i saw you i fell in love, and you smiled because you knew."</p>
                <p class="quote-author">Willian Shakespeare</p><br><br><br>
                <p style="	position:absolute;bottom: 20px;right: 90px;">Powered By <a href="http://www.letsrealize.com"> <span
                                class="quote-author">LetsRealize Holding</span></a> Develop by<a href="http://www.letsrealize.com/portfolio/develop/"> <span
                                class="quote-author">LetsDevelop</span></a></p>

            </footer>
            <!-- /footer -->
        </div>
    </div>
</div>
<!-- /Main Content
================================================== -->

<!-- >> JS
============================================================================== -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset("assets/vendor/jquery.min.js")}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset("assets/vendor/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- Validate -->
<script src="{{asset("assets/vendor/validate.js")}}"></script>
<!-- Countdown -->
<script src="{{asset("assets/vendor/jquery.countdown.min.js")}}"></script>
<!-- Mansonry -->
<script src="{{asset("assets/vendor/imagesloaded.pkgd.min.js")}}"></script>
<script src="{{asset("assets/vendor/masonry.pkgd.min.js")}}"></script>
<!-- Cycle -->

<script src="{{asset("assets/vendor/nivo-lightbox/nivo-lightbox.min.js")}}"></script>
<script src="{{asset("assets/vendor/cycle2.js")}}"></script>
<!-- SmoothScroll -->
<script src="{{asset("assets/vendor/smoothScroll.js")}}"></script>
<!-- Placeholder for IE -->
<script src="{{asset("assets/vendor/placeholders.jquery.min.js")}}"></script>
<!-- Crossbrowser -->
<script src="{{asset("assets/js/cross-browser.js")}}"></script>
<!-- Main Config -->
<script src="{{asset("assets/js/main.js")}}"></script>
<!-- Holder -->
<script src="{{asset("assets/vendor/holder.js")}}"></script>


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="{{asset('assets/vendor/html5shiv.js')}}"></script>
<script src="{{asset('vendor/respond.min.js')}}"></script>


<![endif]-->
@yield('javascript')
<!-- >> /JS
============================================================================= -->

<!-- analytics -->

<!-- /analytics -->
</body>
</html>