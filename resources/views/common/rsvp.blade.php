﻿﻿﻿@extends ('common.layout.app')
@section('content')

    <div class="page-content">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- page title -->
                <h2 class="title1 couple-title1 title1-border">Rsvp</h2>
                <!-- /page title -->
                <div class="row">
                    <!-- RSVP -->
                    <div class="col-sm-4 rsvp-text">
                        <h3 class="title4">Please fll our RSVP Form</h3>
                        <p>Kindly respond by December 29, 2018. We look forward to celebrating with you!</p>
                    </div>
                    <div class="col-sm-8">

                        <!-- Contact Form -->
                        <form class="contactForm" method="POST" role="form" action="{{ url('/guest/register') }}">
                        {{ csrf_field() }}
                        <!-- Form Field -->
                            <div class="form-group">
                                <label for="name">Your name</label>
                                <input type="text" id="name" name="name" class="form-control required"
                                       placeholder="Name" value="{{$name or old('name')}}" required/>
                            </div>
                            <div>
                                @if ($errors->has('name'))
                                    <span class="data-error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /Form Field -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- item -->
                                    <div class="form-group">
                                        <label for="email">Your Email</label>
                                        <input type="email" id="email" name="email" class="form-control required"
                                               placeholder="Email" value="{{$email or old('email')}}" required/>
                                    </div>
                                    <div>
                                        @if ($errors->has('email'))
                                            <span class="data-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <!-- /item -->
                                </div>

                                <div class="col-md-6">
                                    <!-- item -->
                                    <div class="form-group">
                                        <label for="number_guests">How many guests?</label>
                                        <input type="number" min="1" max="10" class="form-control" name="number_guests"
                                               id="guests" placeholder="Number of Guests"
                                               value="{{$number_guests or old('number_guests')}}" required>
                                    </div>
                                    <!-- /item -->
                                </div>
                                <div>
                                    @if ($errors->has('number_guests'))
                                        <span class="data-error">
                                        <strong>{{ $errors->first('number_guests') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <!-- item -->
                            <div class="form-group">
                                <label for="ceremonies">I am attending:</label>
                                <select id="ceremonies" name="ceremonies" class="form-control">

                                    <option value="0">All Events</option>
                                    @foreach($ceremonies as $ceremony)
                                        <option value="{{$ceremony->id}}">{{$ceremony->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                @if ($errors->has('ceremonies'))
                                    <span class="data-error">
                                        <strong>{{ $errors->first('ceremonies') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <!-- Form Field -->

                        @if (Auth::guard('admin')->check())
                            <!-- item -->
                                <div class="form-group">
                                    <label for="table">Name of table</label>
                                    <select id="table" name="table" class="form-control">
                                        <option value="">All Tables</option>
                                        @foreach($table as $tab)
                                            <option value="{{$tab->id}}">{{$tab->nom}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div>
                                    @if ($errors->has('table'))
                                        <span class="data-error">
                <strong>{{ $errors->first('table') }}</strong>
            </span>
                                    @endif
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="message">Message (optional):</label>
                                <textarea class="form-control required" id="message" name="message"
                                          placeholder="Message"></textarea>
                            </div>
                            <!-- /Form Field -->
                            <!-- Form Field -->
                            <div class="form-group form-send">
                                <button type="submit" value="" class="btn btn-1c"><span class="fa fa-paper-plane"> &nbsp;</span>send
                                </button>
                                <!--<input type="submit" value="Send" class="btn btn-1c"/>-->
                            </div>
                            <!-- /Form Field -->
                        </form>
                        <!-- / Contact form -->
                        <!-- Form - Success Message -->
                        <div id="contactSuccess" class="contact-feedback">Your message has been sent!</div>
                        <!-- /Form - Sucess Message -->
                        <!-- Form - Success Message -->
                        <div id="contactSending" class="contact-feedback saving">Sending your message
                            <span>.</span><span>.</span><span>.</span></div>
                        <!-- /Form - Sucess Message -->
                        <!-- Form - Error Message -->
                        <div id="contactError" class="contact-feedback">Error</div>
                        <!-- /form - Error Message -->

                        <div class="form">
                        </div>
                    </div>
                    <!-- /RSVP -->
                </div>
            </div>

        </div>
    </div>
@endsection