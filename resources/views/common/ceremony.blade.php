﻿@extends ('common.layout.app')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- mai title -->
                <h2 class="title1 couple-title1 title1-border">Where to Stay</h2>
                <!-- /main title -->
                <div class="page-intro">
                    <p>A list of sites where you will find hotels for your stay in Kribi.<br/> <br/> <a
                               target="_blank" href="https://www.tripadvisor.co.uk/SmartDeals-g482840-Kribi_South_Region-Hotel-Deals.html">Tripadvisor</a>
                        <br/> <br/> <a target="_blank"
                                href="https://www.booking.com/city/cm/kribi.en-gb.html">Bookings.com</a>
                        <br/> <br/> <a target="_blank"
                                href="https://travel.jumia.com/en-gb/hotels/cameroon/kribi/6319132">Jumia</a></p>
                </div>
                <!-- mai title -->
                <h2 class="title1 couple-title1 title1-border">Ceremony</h2>
                <!-- /main title -->
                <div class="page-intro">
                    <p>Coming to our wedding? Well here’s the details on the big day!<br/> Don’t forget to <a
                                href="{{route('code')}}">RSVP</a>. See you there!</p>
                </div>

                <div class="row row-maps">
                    <!-- Map -->
                    @if(!empty($ceremonies) or $ceremonies !== null)
                        @foreach($ceremonies as $ceremony)
                            <div class="col-sm-6">
                                <div class="ceremony-item">
                                    <h2 class="title3">{{$ceremony->name}}</h2>
                                    <p>{{$ceremony->message}}</p>
                                    <h3 class="title4">{{$ceremony->salle_reception}}</h3>
                                    <p>{{$ceremony->lieu}}</p>
                                    <!-- iframe -->


                                    <div class="iframe-wrapper">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.5847184244585!2d9.907805949725546!3d2.9350053552412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1062fd8259cc1489%3A0x2d59264f74ad318e!2sLa+Maree+Hotel!5e0!3m2!1sfr!2scm!4v1529399307299"
                                                height="220" style="border:0"></iframe>
                                    </div>
                                    <!-- /iframe -->
                                </div>
                            </div>
                    @endforeach
                @endif
                <!-- /Map -->
                </div>
            </div>

        </div>
    </div>
@endsection