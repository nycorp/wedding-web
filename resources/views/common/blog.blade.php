@extends ('common.layout.app')
@section('content')
    <div class="page-content">
        <div class="row">
            <!-- content here -->
            <div class="col-xs-10 col-xs-offset-1">
                <!-- Page title -->
                <h2 class="title1 couple-title1 title1-border">Our Blog</h2>
                <!-- /page title -->
                <!-- SECTION: Blog + Side Column -->
                <div class="section-blog">
                    <div class="row">
                        <!-- blog -->
                        <div class="col-md-9">

                        @if(!empty($blog) or $blog !== null)
                            @foreach($blog as $blo)
                                <!-- Blog Posts -->
                                    <div class="blog-posts">

                                        <!-- item -->
                                        <div class="blog-item">
                                            <div class="row">
                                                <!-- Post thumbnail -->
                                                <div class="col-sm-4">
                                                    <div class="post-thumbnail">
                                                        <a href="{{route('single',$blo->id)}}"> @if(!empty($blo->photo_etape))
                                                                <img src="{{$blo->photo_etape}}" alt="">@endif</a>
                                                    </div>
                                                </div>
                                                <!-- /Post thumbnail -->
                                                <!-- Post content -->
                                                <div class="col-sm-8">
                                                    <h3 class="post-title"><a
                                                                href="{{route('single',$blo->id)}}">{{$blo->title}}</a>
                                                    </h3>
                                                    <div class="post-text">
                                                        <p>
                                                            <a href="{{route('single',$blo->id)}}">{{str_limit($blo->message,200)}}</a>
                                                        </p>
                                                    </div>
                                                    <a href="{{route('single',$blo->id)}}"
                                                       class="btn btn-1c">Read More...</a>
                                                </div>
                                                <!-- /Post content -->
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        @endif
                        <!-- /Blog Posts -->
                            <!-- Pagination container -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- Pagination -->
                                    <div class="section-pagination">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <a href="#">&lt;&lt; Previous</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
														<span class="pagination-numbers">
															<a href="#">1</a>
															<a href="#">2</a>
															<a href="#">3</a>
															<a href="#">4</a>
															<a href="#">5</a>
															<a href="#">6</a>
														</span>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <a href="#">Next &#62;&#62;</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Pagination -->
                                </div>
                            </div>
                            <!-- /Pagination container -->
                        </div>
                        <!-- /blog -->
                        <!-- side column -->
                        <div class="col-md-3">
                            <div class="blog-sidebar">
                                <!-- Tags -->
                                <div class="sidebar-module sidebar-tags">
                                    <h3 class="module-title">Categories</h3>
                                    <!-- tags -->
                                    <div class="tags">
                                        <a href="#">Wedding</a>
                                        <a href="#">How We Meet</a>
                                        <a href="#">Love</a>
                                        <a href="#">Couple</a>
                                        <a href="#">Friends</a>
                                    </div>
                                    <!-- /tags -->
                                </div>
                                <!-- /Tags -->
                                <!-- Archives -->
                                <div class="sidebar-module sidebar-archives">
                                    <h3 class="module-title">Archives</h3>
                                    <!-- tags -->
                                    <ul class="archives-list">
                                        <li><a href="#"><span class="fa fa-caret-right"></span> January 2015</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> September 2014</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> August 2014</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> July 20134</a></li>
                                    </ul>
                                    <!-- /tags -->
                                </div>
                                <!-- /Archives -->
                            </div>
                        </div>
                        <!-- /side-column -->
                    </div>
                </div>
                <!-- /SECTON: Blog + Side Column -->
            </div>
            <!-- /content here -->
        </div>
    </div>
@endsection