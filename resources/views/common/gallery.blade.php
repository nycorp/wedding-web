﻿@extends ('common.layout.app')
@section('content')
            <!-- /header -->
            <!-- content -->
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 couple-container">
                        <!-- Title 1 -->
                        <h1 class="title1 couple-title1 title1-border">Our Pictures</h1>
                        <!-- /Title 1 -->
                        <!-- Gallery -->
                        <div id="mansonry" class="row gallery-wrapper">
                            <!-- item -->
                            @if(!empty($galerie) or $galerie !== null)
                                @foreach($galerie as $galeri)
                                    @if(!empty($galeri->photo))
                                        <div class="mansonry-item col-sm-3">

                                            <a href="{{$galeri->photo}}" data-lightbox-gallery="gallery1">
                                                <img src="{{$galeri->photo}}" alt="">

                                                <div class="picture-legend">{{$galeri->title}}</div>
                                                <div class="mask"></div>
                                            </a>

                                        </div>
                                    @endif
                                @endforeach
                            @endif

                            @if(Auth::guard('guest')->check() or Auth::guard('admin')->check() )
                                <div class="mansonry-item col-sm-3">
                                    <a data-lightbox-gallery="gallery1">

                                        <form action="{{route('selectPicture')}}" method="POST"
                                              enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="user-dashboard-profile">
                                                <div class="profile-thumb member-picture">
                                                    <img src="" id="profile_">
                                                    <label for="picture_">
                                                        Select picture
                                                        <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
                                                        <input type="file" name="picture"
                                                               id="picture_"
                                                               onchange="readURL(this,'profile_')"
                                                               accept="image/*" style="display:none"/>
                                                    </label>

                                                    <div>
                                                        @if ($errors->has('picture'))
                                                            <span class="data-error"><strong>{{ $errors->first('picture') }}</strong></span>
                                                        @endif
                                                    </div>
                                                    &nbsp &nbsp
                                                    <div>
                                                        <input name="title" placeholder="Comment"
                                                               style="width: 200px; text-align: center;font-family: 'Great Vibes', cursive;font-size: 28px; "
                                                               required>
                                                        <div>
                                                            @if ($errors->has('title'))
                                                                <span class="data-error"><strong>{{ $errors->first('title') }}</strong></span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <label for="btn">
                                                        <div class="picture-legend">
                                                            <input type="hidden" value="" name="member_id"/>

                                                            Click here to save!
                                                            <input type="submit" value="Save" id="btn" style="display:none"/>

                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>

                                    </a>
                                </div>
                             @endif
                            <!-- /item -->
                        </div>
                        <!-- /Gallery -->
                        <!-- Pagination container -->
                        <!-- Pagination container -->
                        <div class="row">
                            <div class="col-xs-12">
                                {{ $galerie->links('vendor.pagination.default') }}
                            </div>
                        </div>
                        <!-- /Pagination container -->
                                <!-- /Pagination -->
                            </div>
                        </div>
                        <!-- /Pagination container -->
                    </div>
                </div>

            </div>
            <!-- /content -->
@endsection

@section('javascript')
    {{-- to display image on select --}}

        <script type="text/javascript">
            function readURL(input, image) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        document.getElementById(image).src = e.target.result;
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
@endsection

{{--
@extends ('common.layout.app')
@section('content')
    <div class="page-content">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 couple-container">
                <!-- Title 1 -->
                <h1 class="title1 couple-title1 title1-border">Our Pictures</h1>
                @if(!Auth::guard('guest')->check())
                    <div class="page-intro">
                        <p>Do you want to add picture? <br/> Don’t forget to <a
                                    href="{{route('code')}}">RSVP</a> Or <a
                                    href="{{route('login')}}">Login</a> . See you there!</p>
                    </div>
                @endif
            <!-- /Title 1 -->
                <!-- Gallery -->
                <div id="mansonry" class="row gallery-wrapper">
                    @if(!empty($galerie) or $galerie !== null)
                        @foreach($galerie as $galeri)
                            @if(!empty($galeri->photo))
                                <div class="mansonry-item col-sm-3">

                                    <a href="{{$galeri->photo}}" data-lightbox-gallery="gallery1">
                                        <img src="{{$galeri->photo}}" alt="">

                                        <div class="picture-legend">{{$galeri->title}}</div>
                                        <div class="mask"></div>
                                    </a>

                                </div>
                            @endif
                        @endforeach
                    @endif


                    @if(Auth::guard('guest')->check() or Auth::guard('admin')->check() )
                        <div class="mansonry-item col-sm-3">
                            <a data-lightbox-gallery="gallery1">

                                <form action="{{route('selectPicture')}}" method="POST"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="user-dashboard-profile">
                                        <div class="profile-thumb member-picture">
                                            <img src="" id="profile_">
                                            <label for="picture_">
                                                Select picture
                                                <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
                                                <input type="file" name="picture"
                                                       id="picture_"
                                                       onchange="readURL(this,'profile_')"
                                                       accept="image/*" style="display:none"/>
                                            </label>

                                            <div>
                                                @if ($errors->has('picture'))
                                                    <span class="data-error"><strong>{{ $errors->first('picture') }}</strong></span>
                                                @endif
                                            </div>
                                            &nbsp &nbsp
                                            <div>
                                                <input name="title" placeholder="Comment"
                                                       style="width: 200px; text-align: center;font-family: 'Great Vibes', cursive;font-size: 28px; "
                                                       required>
                                                <div>
                                                    @if ($errors->has('title'))
                                                        <span class="data-error"><strong>{{ $errors->first('title') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>
                                            <label for="btn">
                                                <div class="picture-legend">
                                                    <input type="hidden" value="" name="member_id"/>

                                                    Click here to save!
                                                    <input type="submit" value="Save" id="btn" style="display:none"/>

                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </form>

                            </a>
                        </div>
                @endif
                <!-- item -->

                    <!-- /item -->
                </div>

                <!-- /Gallery -->


                <!-- Pagination container -->
                <div class="row">
                    <div class="col-xs-12">
                        {{ $galerie->links('vendor.pagination.default') }}
                    </div>
                </div>
                <!-- /Pagination container -->
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    --}}
{{-- to display image on select --}}{{--

    <script type="text/javascript">
        function readURL(input, image) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById(image).src = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection--}}
