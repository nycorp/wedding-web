@extends('common.layout.app')
@section('content')
    <div class="page-content">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">

                <!-- page title -->
                <h2 class="title1 couple-title1 title1-border">Rsvp Code</h2>
                <!-- /page title -->
                <div class="row">
                    <!-- RSVP -->
                    <div class="col-sm-4 rsvp-text">
                        <h3 class="title4">Please fll your code and RSVP</h3>
                        <p>Kindly respond by December 29, 2018. We look forward to celebrating with you! Enter Code and
                            RSVP.</p>
                    </div>

                    <div class="col-sm-8">

                        <!-- Contact Form -->
                        <form method="POST" action="{{ route('code') }}">
                        {{ csrf_field() }}
                        <!-- Form Field -->


                            <div class="form-group">
                                <label for="code_commun">Your Code</label>
                                <input type="text" id="code_commun" name="code_commun" class="form-control required"
                                       placeholder="Enter Your Code Here" value="{{ old('code_commun') }}"/>

                                @if ($errors->has('code_commun'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code_commun') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <!-- /Form Field -->


                            <!-- item -->

                            <!-- /item -->
                            <!-- Form Field -->
                            <!-- /Form Field -->
                            <!-- Form Field -->
                            <div class="form-group form-send">
                                <button type="submit" value="" class="btn btn-1c"><span class="fa fa-paper-plane"> &nbsp;</span>Send
                                </button>

                                <!--<input type="submit" value="Send" class="btn btn-1c"/>-->
                            </div>
                            <!-- /Form Field -->
                        </form>
                    </div>

                    <!-- /RSVP -->

                </div>
            </div>
        </div>
    </div>
@endsection