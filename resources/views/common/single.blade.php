﻿﻿@extends ('common.layout.app')
@section('content')
    <div class="page-content">
        <div class="row">
            <!-- content here -->
            <div class="col-xs-10 col-xs-offset-1">
                <!-- SECTION: Blog + Side Column -->
                <div class="section-blog">
                    <div class="row">
                        <!-- blog -->
                        <div class="col-md-9">
                            <!-- Blog Posts -->

                            <div class="blog-single-post">
                                <!-- date -->
                                <div class="blog-single-date-wrapper">
                                    <p class="blog-single-date">{{explode(' ',$blog->created_at)[0]}}</p>
                                </div>
                                <!-- /date -->
                                <!-- post title -->
                                <h2 class="blog-single-title title1-border">{{$blog->title}}</h2>
                                <!-- /post title -->
                                <!-- post image -->
                                <div class="post-header-image">

                                    <img src="{{asset($blog->photo_etape)}}" alt="">

                                </div>
                                <!-- /post-image -->
                                <!-- post content -->
                                <div class="blog-single-content">
                                    <p>{{$blog->message}}</p></div>
                                <!-- /post content -->
                            </div>

                            <!-- /Blog Posts -->
                            <!--=== Blog Comments -->
                            <div class="blog-comments">
                                <h2 class="title2 blog-comments-title">Comments</h2>
                                <!-- Comments Form -->
                                <div class="well">
                                    <h4>Leave a Comment:</h4>
                                    <form role="form" method="POST" action="{{route('commentForm')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="hidden" name="etape_id" value="{{$blog->id}}">
                                            <textarea name="message" class="form-control" rows="3" required></textarea>
                                            @if ($errors->has('message'))
                                                <span class="data-error">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-1c">Submit</button>
                                    </form>
                                </div>

                                <hr>

                                @if(!empty($comment) or $comment !== null)
                                    @foreach($comment as $commen)
                                        <div class="media">
                                            <a class="pull-left">
                                                <img class="media-object" src="{{asset("assets/img/comments1.jpg")}}"
                                                     alt="">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading">{{ \App\Guest::where('id',$commen->guests_id)->first()->name}}
                                                    <small>{{$commen->date}}</small>
                                                </h4>
                                                {{$commen->message}}
                                            </div>
                                        </div>
                                    @endforeach
                                <!-- Pagination container -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {{ $comment->links('vendor.pagination.default') }}
                                            </div>
                                        </div>
                                @endif
                            </div>
                            <!-- === /Blog Comments -->

                        </div>
                        <!-- /blog -->

                        <!-- side column -->
                        <div class="col-md-3">
                            <div class="blog-sidebar">
                                <!-- Tags -->
                                <div class="sidebar-module sidebar-tags">
                                    <h3 class="module-title">Categories</h3>
                                    <!-- tags -->
                                    <div class="tags">
                                        <a href="#">Wedding</a>
                                        <a href="#">How We Meet</a>
                                        <a href="#">Love</a>
                                        <a href="#">Couple</a>
                                        <a href="#">Friends</a>
                                    </div>
                                    <!-- /tags -->
                                </div>
                                <!-- /Tags -->
                                <!-- Archives -->
                                <div class="sidebar-module sidebar-archives">
                                    <h3 class="module-title">Archives</h3>
                                    <!-- tags -->
                                    <ul class="archives-list">
                                        <li><a href="#"><span class="fa fa-caret-right"></span> January 2015</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> September 2014</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> August 2014</a></li>
                                        <li><a href="#"><span class="fa fa-caret-right"></span> July 20134</a></li>
                                    </ul>
                                    <!-- /tags -->
                                </div>
                                <!-- /Archives -->
                            </div>
                        </div>
                        <!-- /side-column -->
                    </div>
                </div>
                <!-- /SECTON: Blog + Side Column -->
            </div>
            <!-- /content here -->
        </div>
    </div>
@endsection