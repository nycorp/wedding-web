﻿@extends ('common.layout.app')
@section('content')
    <div class="page-content clearfix">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- Section: About Us -->
                <div class="section-about-us">
                    <!-- Title 1 -->
                    <h1 class="title1 couple-title1 title1-border">About Us</h1>
                    <!-- /Title 1 -->
                    <div class="row">
                    @if(!empty($couple) or $couple !== null)
                        @foreach($couple as $coupl)
                            <!-- Groom -->
                                <div class="col-sm-6">
                                    <div class="about-item">

                                        <div class="about-item-picture">
                                            <img src="{{asset($coupl->photo)}}" alt="">
                                        </div>

                                        <h2 class="about-item-title">{{$coupl->lastName}}</h2>
                                        <h3 class="about-item-subtitle">{{$coupl->title}}</h3>
                                        <div class="about-item-content">
                                            <p>{{$coupl->comment}}</p>

                                        </div>
                                    </div>

                                </div>
                        @endforeach
                    @endif
                    <!-- /Groom -->

                        <!-- Bride -->

                        <!-- /Bride -->
                    </div>


                </div>

                <!-- /Section: About Us -->
                <!-- Section: Timeline -->

                <div class="section-timeline">
                    <!-- Title 1 -->
                    <h2 class="title1 couple-title1 title1-border">Our Timeline</h2>
                    <!-- /Title 1 -->
                    <!-- timeline -->
                    @if(!empty($timeLine) or $timeLine !== null)
                        @foreach($timeLine as $timeLin)

                            <ul class="timeline">
                                <!-- item -->
                                <li @if($timeLin->couples_id > 1)class="timeline-inverted" @endif>
                                    <!-- badge -->
                                    <div class="timeline-badge">
                                        <span class="timeline-date">{{explode('/',$timeLin->date)[0].'/'.explode('/',$timeLin->date)[1]}}</span>
                                        <span class="timeline-year">{{explode('/',$timeLin->date)[2]}}</span>
                                    </div>
                                    <!-- /badge -->
                                    <!-- panel -->
                                    <div class="timeline-panel">
                                        <!-- item heading -->
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">{{$timeLin->title}}</h4>
                                            <p>
                                                <small class="text-muted">{{$timeLin->sous_title}}</small>
                                            </p>
                                        </div>
                                        <!-- /item heading -->
                                        <!-- item body -->
                                        <div class="timeline-body">
                                            @if(!empty($timeLin->comment))<p> {{$timeLin->comment}}</p><br> @endif
                                            @if(!empty($timeLin->photo))
                                                <img src="{{asset($timeLin->photo)}}" alt="">
                                            @endif
                                        </div>
                                        <!-- /item body -->
                                    </div>
                                    <!-- /panel -->
                                </li>
                                <!-- /item -->
                                <!-- item -->

                            </ul>
                    @endforeach
                @endif
                <!-- /timeline -->
                </div>
                <!-- /Section: Timeline -->
            </div>
        </div>
    </div>
@endsection



