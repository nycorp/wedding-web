<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css">
        html {
            background: #FAF7F2;
            background-image: url(https://s3.postimg.org/s1n3ji1ur/paper_fibers_2_X.png);
            box-sizing: border-box;
            font-family: 'Lato', sans-serif;
            font-size: 14px;
            font-weight: 400;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }

        .u-clearfix:before,
        .u-clearfix:after {
            content: " ";
            display: table;
        }

        .u-clearfix:after {
            clear: both;
        }

        .u-clearfix {
            *zoom: 1;
        }

        .subtle {
            color: #aaa;
        }

        .card-container {
            margin: 25px auto 0;
            position: relative;
            width: 692px;
        }

        .card {
            background-color: #fff;
            padding: 30px;
            position: relative;
            box-shadow: 0 0 5px rgba(75, 75, 75, .07);
            z-index: 1;
        }

        .card-body {
            display: inline-block;
            float: left;
            width: 310px;
        }

        .card-number {
            margin-top: 15px;
        }

        .card-circle {
            border: 1px solid #aaa;
            border-radius: 50%;
            display: inline-block;
            line-height: 22px;
            font-size: 12px;
            height: 25px;
            text-align: center;
            width: 25px;
        }

        .card-author {
            display: block;
            font-size: 12px;
            letter-spacing: .5px;
            margin: 15px 0 0;
            text-transform: uppercase;
        }

        .card-title {
            font-family: 'Cormorant Garamond', serif;
            font-size: 60px;
            font-weight: 300;
            line-height: 60px;
            margin: 10px 0;
        }

        .card-description {
            display: inline-block;
            font-weight: 300;
            line-height: 22px;
            margin: 10px 0;
        }

        .card-read {
            cursor: pointer;
            font-size: 14px;
            font-weight: 700;
            letter-spacing: 6px;
            margin: 5px 0 20px;
            position: relative;
            text-align: right;
            text-transform: uppercase;
        }

        .card-read:after {
            background-color: #b8bddd;
            content: "";
            display: block;
            height: 1px;
            position: absolute;
            top: 9px;
            width: 75%;
        }

        .card-tag {
            float: right;
            margin: 5px 0 0;
        }

        .card-media {
            float: right;
        }

        .card-shadow {
            background-color: #fff;
            box-shadow: 0 2px 25px 2px rgba(0, 0, 0, 1), 0 2px 50px 2px rgba(0, 0, 0, 1), 0 0 100px 3px rgba(0, 0, 0, .25);
            height: 1px;
            margin: -1px auto 0;
            width: 80%;
            z-index: -1;
        }
    </style>
    <title>Recipe Card</title>


    <link rel='stylesheet prefetch'
          href='https://fonts.googleapis.com/css?family=Cormorant+Garamond:300|Lato:300,400,700'>

    <link rel="stylesheet" href="css/style.css">


</head>

<body>

<div class="card-container">
    <div class="card u-clearfix">
        <div class="card-body">
            <span class="card-number card-circle subtle">01</span>
            <span class="card-author subtle"> Please login in your email address by clicking the link below</span>
            <meta itemprop="name" content="Confirm Email"
                  style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"/>
            <br>
            <br>
            <table width="100%" cellpadding="0" cellspacing="0"
                   style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block"
                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                        valign="top">
                        <b>Username : </b> <b style="color: green">{{$credentials['email']}}</b>
                        <br><b>Password : </b> <b style="color: green">{{$credentials['password']}}</b>
                        <br><b>Code Secret : </b> <b style="color: green">{{$credentials['code']}}</b>
                    </td>
                </tr>
                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" itemprop="handler"
                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                        valign="top">
                        <a href="{{route('homes')}}" class="btn-primary" itemprop="url">
                            Accedez À Notre Site
                        </a>
                    </td>
                </tr>
                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

                </tr>
            </table>

            <span class="card-description subtle">
                <table>
                    <tr>
                    <td><img src="data:image/png;base64, {!!base64_encode(QrCode::format('png')->color(92, 69, 63)->size(100)->generate($credentials['code']))!!}"/>
             </td>
                        <td>
                            We may need to send you critical information about our service and it is
                                        important that we have an accurate email address.
                        </td>
                </tr>
                </table>
                </span>
            <div class=" card-read"> &mdash; Wedding</div>
            <span class="card-tag card-circle subtle">C</span>
        </div>
        <img src="{{asset("assets/img/InvitationImg.jpg")}}" alt="" class="card-media"/>
    </div>
    <div class="card-shadow"></div>
</div>


</body>
</html>



