@extends('common.layout.app')
@section('content')
    <div class="page-content">

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- page title -->
                <h2 class="title1 couple-title1 title1-border">Rsvp login</h2>
                <!-- /page title -->
                <div class="row">
                    <!-- RSVP -->
                    <div class="col-sm-4 rsvp-text">
                        <h3 class="title4">Please fll our RSVP Login Form</h3>
                        <p>Kindly respond by December 29, 2018. We look forward to celebrating with you!</p>
                    </div>
                    <div class="col-sm-8">

                        <!-- Contact Form -->
                        <form method="POST" action="{{ url('/guest/login') }}">
                        {{ csrf_field() }}
                        <!-- Form Field -->


                            <div class="form-group">
                                <label for="email">Your Email</label>
                                <input type="email" id="email" name="email" class="form-control required"
                                       placeholder="Email" value="{{ old('email') }}"/>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Your password</label>
                                <input type="password" id="password" name="password" class="form-control required"
                                       placeholder="Password"/>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /Form Field -->


                            <!-- item -->

                            <!-- /item -->
                            <!-- Form Field -->
                            <!-- /Form Field -->
                            <!-- Form Field -->
                            <div class="form-group form-send">
                                <button type="submit" value="" class="btn btn-1c"><span class="fa fa-paper-plane"> &nbsp;</span>login
                                </button>


                                <a class="btn btn-link" href="{{ url('/guest/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                                <!--<input type="submit" value="Send" class="btn btn-1c"/>-->
                            </div>
                            <!-- /Form Field -->
                        </form>

                        <div class="form">
                        </div>
                    </div>
                    <!-- /RSVP -->
                </div>
            </div>

        </div>
    </div>
@endsection