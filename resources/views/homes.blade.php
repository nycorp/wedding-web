@extends ('common.layout.app')
@section('content')
    <div class="page-content page-home">
        <!-- home logo -->
        <div class="row">
            <div class="home-logo col-sm-8 col-sm-offset-2">
                <p class="couple-title">
                    @if(!empty($couple_name) or $couple_name !== null)

                        {{$couple_name}}

                    @endif</p>
            </div>
        </div>
        <!-- /home-logo -->
        <!-- Countdown container -->
        <div class="row">
            <div class="countdown-container">
                <!-- icon -->
                <div class="countdown-icon">
                    <img src="{{asset("assets/img/ico-heart2.png")}}" alt="">
                </div>
                <!-- /icon -->
                <!-- countdown title top -->
                <h2 class="countdown-title countdown-title1"><span class="countdown-title-borders">December 29, 2018 in Kribi, Cameroon</span>
                </h2>
                <!-- /countdown title top -->
                <!-- countdown ( Edit it on main.js )-->
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div id="countdown" class="row"></div>
                    </div>
                </div>
                <!-- /countdown -->
                <!-- countdown title bot -->
                <h3 class="countdown-title countdown-title2"><span
                            class="countdown-title-borders">Until we get Married!</span></h3>
                <!-- countdown title /bot -->
            </div>
        </div>
        <!-- /Countdown Container -->
    </div>
@endsection
@section('javascript')
    <script>
        //=====>  Countdown (Edit this with your own date)  <====
        $("#countdown").countdown("2018/12/29 15:10:56", function (event) {
            var $this = $(this).html(event.strftime(''
                + '<div class="countdown-col-wrapper col-xs-3"><div class="countdown-col"><span class="countdown-time"> %-D </span> Days </div></div> '
                + '<div class="countdown-col-wrapper col-xs-3"><div class="countdown-col"><span class="countdown-time"> %H </span> Hours </div></div>'
                + '<div class="countdown-col-wrapper col-xs-3"><div class="countdown-col"><span class="countdown-time"> %M </span> Minutes </div></div>'
                + '<div class="countdown-col-wrapper col-xs-3"><div class="countdown-col"><span class="countdown-time"> %S </span> Seconds </div></div>'));
        });
    </script>
@endsection