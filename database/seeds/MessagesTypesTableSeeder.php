<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessagesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('message_types')->insert([
            'name' => 'text',
            'value' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
