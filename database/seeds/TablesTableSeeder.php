<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->delete();
        DB::table('tables')->insert([
            'nom' => 'Table1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
