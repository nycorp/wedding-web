<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class EtapeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('etapes')->delete();
        DB::table('etapes')->insert([
            'title' => 'Engagement',
            'message' => 'When I think of the future, I like to picture us as two trees planted side-by-side, our roots growing togther more firmly as the years go by, and our children sprouting like seedlings around us. ',
            'photo_etape' => 'assets/img/IMG-20180613-WA0015.jpg',
            'ceremonies_id' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('etapes')->insert([
            'title' => ' buffet',
            'message' => '',
            'photo_etape' => 'assets/img/buffets.jpg',
            'ceremonies_id' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('etapes')->insert([
            'title' => 'Wedding Cake',
            'message' => '',
            'photo_etape' => 'assets/img/cake1.jpg',
            'ceremonies_id' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
