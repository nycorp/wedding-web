<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CeremonyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ceremonies')->delete();
        DB::table('ceremonies')->insert([
            'name' => 'Main Ceremony',
            'message' => 'We like everything on the wedding day be perfect!',
            'salle_reception' => 'December 29th, 2018 at 1.28pm',
            'lieu' => 'Hotel tara plage Kribi',
            'latitude' => '0',
            'longitude' => '0',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('ceremonies')->insert([
            'name' => 'Wedding Party',
            'message' => 'We have reserved a great hall!',
            'salle_reception' => ' December 29th, 2018 at 7.00pm ',
            'lieu' => 'La maree Hotel Kribi',
            'latitude' => '0',
            'longitude' => '0',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }

}
