<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Eloquent::unguard();

        DB::table('guests')->delete();
        DB::table('guests')->insert([
            'name' => 'linalets',
            'code' => '11linaQG',
            'email' => 'lina@gmail.com',
            'password' => bcrypt('password'),
            'message' => 'Felicitation au couple',
            'number_guests' => '2',
            'present' => 'false',
            'table_id'=>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('guests')->insert([
            'name' => 'Yann Yvan',
            'code' => '11linaQG',
            'present' => 'false',
            'email' => 'yann@gmail.com',
            'password' => bcrypt('password'),
            'message' => 'Felicitation au couple',
            'number_guests' => '2',
            'table_id'=>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('guests')->insert([
            'name' => 'David Maxime',
            'code' => '11linaQG',
            'present' => 'false',
            'email' => 'david@gmail.com',
            'password' => bcrypt('password'),
            'message' => 'Felicitation au couple',
            'number_guests' => '2',
            'table_id'=>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
