<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        DB::table('admins')->insert([
            'name' => 'linalets',
            'email' => 'nrespound@gmail.com',
            'password' => bcrypt('letsrealize'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
