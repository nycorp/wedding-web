<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CoupleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('couples')->delete();
        DB::table('couples')->insert([
            'firstName' => 'Duplexe',
            'title' => 'The Groom',
            'lastName' => 'Duplexe',
            'photo' => 'assets/img/groom.jpg',
            'comment' => '',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('couples')->insert([
            'firstName' => 'Wendy',
            'title' => 'The Bride',
            'lastName' => 'Wendy',
            'photo' => 'assets/img/bride.jpg',
            'comment' => 'When I met Duplexe for the first time, I was immediately at ease with him. It was like that I had always known him. I thought, "I just met someone special" ...',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);


    }
}
