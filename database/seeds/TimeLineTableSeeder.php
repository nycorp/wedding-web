<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class TimeLineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('time_lines')->delete();
        DB::table('time_lines')->insert([
            'title' => 'Day we meet each other',
            'sous_title' => 'A special day for us!',
            'couples_id' => '1',
            'photo' => 'assets/img/recipe2.jpg',
            'date' => '19/08/2016',
            'comment' => 'Duplexe was in London for work so a friend put us in touch.We met at Victoria station in London for a quick "blind date", while Wendy was waiting for her bus to Paris. The flow went by and we talked to each other every day after ...',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

        DB::table('time_lines')->insert([
            'title' => 'Our Engagement',
            'sous_title' => 'A funny day',
            'couples_id' => '2',
            'photo' => 'assets/img/gallery_1529403210.png',
            'date' => '28/05/2018',
            'comment' => 'We went to Dallas to see her family and "celebrate graduation from the cousin of Duplexe", which was actually a surprise party for Wendy.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('time_lines')->insert([
            'title' => 'Our Wedding Day',
            'sous_title' => '',
            'couples_id' => '1',
            'photo' => '',
            'date' => '29/12/2018',
            'comment' => 'Will planned a ceremony in a hotel by the sea to watch the waves of the sea of 29 December',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
    }
}
