<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(AdminsTableSeeder::class);

        $this->call(TablesTableSeeder::class);

        $this->call(GuestsTableSeeder::class);
        $this->call(CeremonyTableSeeder::class);

        $this->call(CoupleTableSeeder::class);
        $this->call(TimeLineTableSeeder::class);
        $this->call(EtapeTableSeeder::class);
        $this->call(GalleryTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MessagesTypesTableSeeder::class);

    }
}
