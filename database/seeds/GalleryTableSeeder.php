<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galeries')->delete();
        DB::table('galeries')->insert([
            'title' => 'love!',
            'photo' => 'assets/img/gallery_1529402414.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'yahhh!',
            'photo' => 'assets/img/im1.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'So beautiful!',
            'photo' => 'assets/img/gallery_1529414215.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'Cool!',
            'photo' => 'assets/img/gallery_1529402638.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'ih!!',
            'photo' => 'assets/img/gallery_1529402874.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'Beautiful!',
            'photo' => 'assets/img/gallery_1529403210.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'So...',
            'photo' => 'assets/img/gallery_1529412041.png',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'Love is...',
            'photo' => 'assets/img/img2.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'yes oh!',
            'photo' => 'assets/img/img3.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'congratulation',
            'photo' => 'assets/img/img4.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);
        DB::table('galeries')->insert([
            'title' => 'congratulation we are beautiful',
            'photo' => 'assets/img/img5.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]);

    }
}
