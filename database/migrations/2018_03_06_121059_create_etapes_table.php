<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtapesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etapes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('message', 10000);
            $table->string('photo_etape');
            $table->integer('ceremonies_id')->unsigned();
            $table->foreign('ceremonies_id')->references('id')->on('ceremonies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etapes', function (Blueprint $table) {
            $table->dropForeign('etapes_ceremonies_id_foreign');
        });
        Schema::dropIfExists('etapes');
    }
}
