<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCeremoniesGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ceremonies_guests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ceremonies_id')->unsigned();
            $table->foreign('ceremonies_id')->references('id')->on('ceremonies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->unsignedBigInteger('guests_id')->unsigned();
            $table->foreign('guests_id')->references('id')->on('guests')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attending');
    }
}
