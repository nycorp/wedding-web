<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gift_lists_id')->unsigned();
            $table->foreign('gift_lists_id')->references('id')->on('gift_lists')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->unsignedBigInteger('guests_id')->unsigned();
            $table->foreign('guests_id')->references('id')->on('guests')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
