<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('message');
            $table->unsignedBigInteger('guests_id')->unsigned();
            $table->foreign('guests_id')->references('id')->on('guests')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->Integer('etapes_id')->unsigned();
            $table->foreign('etapes_id')->references('id')->on('etapes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
