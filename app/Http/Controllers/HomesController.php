<?php

namespace App\Http\Controllers;

use App\Couple;

class HomesController extends Controller
{
    function index()
    {

        $couples = Couple::select('lastName')->get();
        if (empty($couples))
            return view('homes')->with('couples', '');
        $name = array();
        foreach ($couples as $couple) {
            array_push($name, $couple->lastName);
        }

//dd($couples);
        return view('homes')->with('couple_name', join(' & ', $name));
    }
}
