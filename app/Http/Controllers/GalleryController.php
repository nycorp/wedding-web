<?php

namespace App\Http\Controllers;

use App\Etape;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GalleryController extends Controller
{
    public function index()
    {
        $etapes = Etape::All();
        $galerie = Gallery::orderByDesc('created_at')->paginate(20);
        return view('common.gallery')->with(['galerie' => $galerie]);
    }


    function createImage(Request $request)
    {
        $this->validate($request, [
            'picture' => 'bail|required',
            'title' => 'bail|required',
        ]);

        $gallery = new Gallery();
        $gallery->title = $request->input('title');

        if (Input::file('picture') != null) {
            $file = Input::file('picture')->getRealPath();
            $file_name = 'gallery_' . time();
            $gallery->photo = $this->savePicture($file, $file_name);
        }

        if (!$gallery->save())
            return redirect()->back();
        return redirect()->route('gallery');
    }


}
