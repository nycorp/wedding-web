<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function savePicture($file, $file_name)
    {
        $path = 'assets/img/' . $file_name . '.png';
        $img = Image::make($file);
        $img->resize(500, 500);
        $img->save(public_path($path));
        return $path;
    }

    function test()
    {
        return view('guest.auth.verification_code')->with('credentials', ['email' => 'leticia.kegna@letsrealize.com', 'password' => 'linalets', 'code' => '11qwwer']);
    }
}
