<?php

namespace App\Http\Controllers;

use App\Ceremony;

class CeremonyController extends Controller
{
    function index()
    {
        $ceremonies = Ceremony::select()->get();
        //  dd($ceremonies);
        return view('common.ceremony')->with('ceremonies', $ceremonies);
    }
}
