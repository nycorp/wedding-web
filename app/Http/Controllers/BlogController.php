<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Etape;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    function index()
    {
        $blog = Etape::select()->get();
        //dd($blog);
        return view('common.blog')->with('blog', $blog);
    }

    function getBlog($id)
    {
        return self::reload($id);
    }

    function registerComment(Request $request)
    {
        $data['message'] = $request->input('message');

        $this->validate($request, [
            'message' => 'bail|required',
        ]);

        $comment = new Comment;
        $comment->message = $request->input('message');
        $comment->date = Carbon::now();
        $comment->guests_id = Auth::guard('guest')->user()->id;
        $comment->etapes_id = $request->input('etape_id');
        $comment->save();
        $request['step_id'] = $request->input('etape_id');
        return redirect()->route('single', $request);
    }

    function reload($id)
    {
        $blog = Etape::where('id', $id)->first();
        if (empty($blog)) return redirect()->back();
        $comment = Comment::where('etapes_id', $blog->id)->orderby('date', 'DESC')->paginate(5);
        return view('common.single')->with(['comment' => $comment, 'blog' => $blog, 'etape_id' => $blog->id]);
    }
}
