<?php

namespace App\Http\Controllers;

use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    function Verify(Request $request)
    {
        $code=json_decode($request->getContent(),true);

        $validator = Validator::make($code, [
            'code' => 'required|exists:guests,code',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false,
                'message' => 1601,
              ]);
        }
        $guest=Guest::where('code',$code)->first();
        $guest->present=true;
        $guest->save();
        return response()->json(['status' => false,
            'message' => 1600,
            'guest' => $guest]);

    }


}
