<?php

namespace App\Http\Controllers\Dashboard;

use App\Couple;
use App\Http\Controllers\Controller;
use App\TimeLine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TimeLineController extends Controller
{
    public function index()
    {
        $timelines = TimeLine::select()->get();
        $couple = Couple::select()->get();
        return view('admin.timeline')->with(['timelines' => $timelines, 'couple' => $couple]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'bail|required',
            'title' => 'bail|required',
            'couple_id'=>'bail|required'
        ]);

        $timeLine = TimeLine::where('id', $request->id)->first();
        if ($timeLine == null) return redirect()->refresh();
        if (Input::file('picture') != null) {
            $file = Input::file('picture')->getRealPath();
            $file_name = 'timeline_' . time();
            $timeLine->photo = $this->savePicture($file, $file_name);
        }

        $timeLine->title = $request->title;
        $timeLine->sous_title = $request->sub_title;
        $timeLine->couples_id = $request->couple_id;
        $timeLine->comment = $request->comment;
        $timeLine->date = Carbon::now();
        $timeLine->save();
        return redirect()->route('admin.timeline');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'bail|required',
            'title' => 'bail|required',
            'couple_id'=>'bail|required'
        ]);

        $timeLine = new TimeLine();
        if (Input::file('picture') != null) {
            $file = Input::file('picture')->getRealPath();
            $file_name = 'timeline_' . time();
            $timeLine->photo = $this->savePicture($file, $file_name);
        }

        $timeLine->title = $request->title;
        $timeLine->sous_title = $request->sub_title;
        $timeLine->couples_id = $request->couple_id;
        $timeLine->comment = $request->comment;
        $timeLine->date = Carbon::now();
        $timeLine->save();
        return redirect()->route('admin.timeline');
    }
}
