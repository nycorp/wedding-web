<?php

namespace App\Http\Controllers\Dashboard;

use App\Guest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestController extends Controller
{
    public function index()
    {
        $guest = Guest::All();
        return view('admin.request_guest')->with(['guest' => $guest]);
    }

}
