<?php

namespace App\Http\Controllers\Dashboard;

use App\Couple;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CoupleController extends Controller
{
    public function index()
    {
        $couple = Couple::select()->get();
        return view('admin.couple')->with(['couple' => $couple]);
    }

    public function store(Request $request)
    {
        //decode receive information
        //dd($request->all());
        $this->validate($request, [
            'id' => 'bail|required',
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'comment' => 'bail|required',
        ]);

        $couple = Couple::where('id', $request->id)->first();
        if ($couple == null) return redirect()->back();
        $couple->firstName = $this->format_name($request->input('first_name'));
        $couple->lastName = $this->format_name($request->input('last_name'));
        $couple->comment = $request->input('comment');
        if (Input::file('picture') != null) {
            $file = Input::file('picture')->getRealPath();
            $file_name = 'half_' . $couple->id;
            $couple->photo = $this->savePicture($file, $file_name);
        }
        $couple->save();
        return redirect()->route("admin.couple");
    }

    public function format_name($name){
        return ucwords(trim($name));
    }
}
