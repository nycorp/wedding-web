<?php

namespace App\Http\Controllers\Dashboard;

use App\Guest;
use App\Http\Controllers\Controller;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TableController extends Controller
{
    public function createTable()
    {
        return view('admin.create_table');
    }
    public function storeTable(Request $request)
    {

        $table = new Table;
        $table->nom = $request->input('nom');
        $table->save();

        return view('admin.home');
    }

    public function edit($id )
    {

        $table = Table::where($id)->get();

        return view('admin.update_table')->with(['table'=> $table]);
    }


    public function delete($id){
        dd($id);
    }
    public function assignerTable(){
        $guest = Guest::select()->get();
        $table = Table::select()->get();

        return view('admin.assigner')->with(['guest'=> $guest,'table'=>$table]);
    }


    public function afficheTable(){
        $table = Table::select()->get();
        return view('admin.liste_table')->with([ 'table' => $table]);
    }
    public function update($id, Request $request)
    {    $request->validate([
        'nom' => 'required|unique'

    ]);

        $table = Table::findOrFail($id);

        $table->name = $request->get('nom');

        $table->save();

        return \Redirect::route('admin.edit', [$table->id])->with('message', 'User has been updated!');

    }
}
