<?php

namespace App\Http\Controllers;

use App\Attending;
use App\Ceremony;
use App\Table;
use App\User;
use Illuminate\Http\Request;

class RsvpController extends Controller
{
    function index()
    {
        $ceremonies = Ceremony::All();
        $table = Table::All();
        return view('common.rsvp')->with(['ceremonies' => $ceremonies, 'table' => $table]);
    }

    function getCode()
    {
        return view('common.code_commun');

    }

    function codeCommun(Request $request)
    {

        $code = $request->input('code_commun');
        $code1 = User::all()->first();


//dd($code1->code_commun);
        if (isset($code1->code_commun)) {
            if (strcmp($code, $code1->code_commun) == 0) {
                return redirect(route('rsvp'));
            } else {
                if (empty($code))
                    $this->validate($request, [
                        'code_commun' => 'bail|required'
                    ]);
                else
                    return redirect()->back()->withErrors(['code_commun' => 'The code is invalid']);
            }
        }

    }
}
