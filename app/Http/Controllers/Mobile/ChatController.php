<?php

namespace App\Http\Controllers\Mobile;

use App\Guest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class ChatController extends Controller
{
    private $user;

    public function __construct(Guest $user)
    {
        Config::set('jwt.user', Guest::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Guest::class,
        ]]);
        $this->user = $user;
    }

    public function get_contacts()
    {
        $users = Guest::all();
        if (empty($this->user)) {
            return response()->json(['status' => false,
                'message' => 1501]);
        }
        return response()->json(['status' => true,
            'message' => 1500,
            'user' => $users]);
    }
}
