<?php

namespace App\Http\Controllers;

use App\Couple;
use App\TimeLine;

class AboutController extends Controller
{
    function index()
    {
        $couple = Couple::select()->get();
        $timeLine = TimeLine::select()->get();
        //dd($timeLine);
        return view('common.about')->with(['couple' => $couple, 'timeLine' => $timeLine]);
    }
}
