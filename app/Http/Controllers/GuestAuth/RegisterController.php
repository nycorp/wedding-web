<?php

namespace App\Http\Controllers\GuestAuth;

use App\Ceremony;
use App\Guest;
use App\Http\Controllers\Controller;
use App\Mail\SendVerificationCode;
use Dirape\Token\Token;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('guest.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {//dd($data);
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:guests',
            'message' => 'bail|max:255',
            'number_guests' => 'required|numeric',
            'table' => 'numeric',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return Guest
     */
    protected function create(array $data)
    {

        $token = new Token();
        $pwd = str_random(8);
        $data['password'] = $pwd;
        $data['code'] = $token->Unique('guests', 'code', 20);


        $guest = Guest::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'message' => $data['message'],
            'number_guests' => $data['number_guests'],
            'code' => $data['code'],
            'table_id'=>$data['table'],
            'password' => bcrypt($pwd),

        ]);
        //dd($guest);
        $query = array();
        if ($data['ceremonies'] < 1) {//build query
            foreach (Ceremony::all() as $event) {
                array_push($query, [
                    'guests_id' => $guest->id,
                    'ceremonies_id' => $event->id,

                ]);
            }
        } else {
            array_push($query, [
                'guests_id' => $guest->id,
                'ceremonies_id' => $data['ceremonies'],

            ]);
        }

        //execute query
        DB::table('ceremonies_guests')->insert($query);


        Mail::to($data['email'])->send(new SendVerificationCode($data,
            route('homes')));
        return $guest;
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('guest');
    }
}
