<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CeremoniesGuests extends Model
{
    protected $table = 'ceremonies_guests';
}
