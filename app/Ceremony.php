<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ceremony extends Model
{
    protected $table = 'ceremonies';
}
