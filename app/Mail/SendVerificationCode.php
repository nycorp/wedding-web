<?php

namespace App\Mail;

use App\Couple;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendVerificationCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $credentials;
    protected $verification_link;

    public function __construct($credentials, $link)
    {
        $this->credentials = $credentials;
        $this->verification_link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $couples = Couple::select('lastName')->get();
        if (empty($couples))
            return view('homes')->with('couples', '');
        $name = array();
        foreach ($couples as $couple) {
            array_push($name, $couple->lastName);
        }

        $address = "nrespound@gmail.com";
        $name = join(' & ', $name) . ' Wedding';
        $subject = 'Invitation';

        return $this->view('guest.auth.verification_code')
            ->from($address, $name)
            ->subject($subject)
            ->with(['credentials' => $this->credentials, 'url' => $this->verification_link]);
    }

}