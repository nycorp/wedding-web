<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
   return view('lolo');
});
Route::get('verification', function() {
     $data['name']='Yann Yvan Long text pour voir';
            $data['email']='yann@email.com';
             $data['message']='??????';
             $data['number_guests']='0';
             $data['code']='lkcjxklcj';
             $data['password']='1234567890';
    return view('guest.auth.verification_code')
        ->with(['credentials' => $data, 'url' => route('homes')]);
})->name('verification');

Route::get('/', 'HomesController@index')->name('homes');

Route::get('about', 'AboutController@index')->name('about');

Route::get('gallery', 'GalleryController@index')->name('gallery');

Route::get('ceremony', 'CeremonyController@index')->name('ceremony');



Route::get('blog', 'BlogController@index')->name('blog');

Route::get('registry', 'RegistryController@index')->name('registry');

Route::get('rsvp', 'RsvpController@index')->name('rsvp');
Route::get('code', 'RsvpController@getCode')->name('code');
Route::post('code', 'RsvpController@codeCommun')->name('code');

Route::get('single/{id}', 'BlogController@getBlog')->name('single');
Route::get('testV', 'Controller@test')->name('testV');




Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'guest'], function () {
    Route::get('/login', 'GuestAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'GuestAuth\LoginController@login');
    Route::post('/logout', 'GuestAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'GuestAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'GuestAuth\RegisterController@register');

    Route::post('/password/email', 'GuestAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'GuestAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'GuestAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'GuestAuth\ResetPasswordController@showResetForm');
});

Route::middleware("auth:guest")->group(function () {
    Route::get('commentForm', 'BlogController@registerComment')->name('commentForm');
    Route::post('commentForm', 'BlogController@registerComment')->name('commentForm');
    Route::get('selectPicture', 'GalleryController@createImage')->name('selectPicture');
    Route::post('selectPicture', 'GalleryController@createImage')->name('selectPicture');
});

Route::middleware("auth:admin")->group(function () {

});