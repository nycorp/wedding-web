<?php

Route::get('/couple','Dashboard\CoupleController@index')->name('couple');
Route::post('/couple','Dashboard\CoupleController@store');
Route::get('/timeline','Dashboard\TimeLineController@index')->name('timeline');
Route::get('/guest','Dashboard\RequestController@index')->name('guest');
Route::post('/timeline/update','Dashboard\TimeLineController@update');
Route::post('/timeline/store','Dashboard\TimeLineController@store');
Route::get('/table','Dashboard\TableController@createTable')->name('table');

Route::get('table/edit/{$id}','Dashboard\TableController@edit')->name('edit');
Route::get('/assigner/','Dashboard\TableController@assignerTable')->name('assigner');

Route::get('/liste','Dashboard\TableController@afficheTable')->name('liste');
Route::post('/update','Dashboard\TableController@update')->name('update');

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home');

